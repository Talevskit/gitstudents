package service;

import java.util.List;
import java.util.ArrayList;

import model.Student;

public class StudentService {

	List<Student> students = new ArrayList<Student>();
	
	
	public List<Student> findAll(){
		return students;
	}

	public Student addStudent(Student student) {
		students.add(student);
		return student;
	}

	public Student removeStudent(int id) {
		for (Student student : students) {
			if (student.getId() == id) {
				students.remove(student);
				return student;
			}
		}
		return null;
	}

	public Student getStudent(int id) {
		for (Student student : students) {
			if (student.getId() == id) {
				return student;
			}
		}
		return null;
	}

}
