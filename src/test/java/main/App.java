package main;

import java.util.List;

import model.Student;
import service.StudentService;

public class App {

	public static void main(String[] args) {
		StudentService service = new StudentService();
		
		Student s1 = new Student(1,"Pero","Perov");
		Student s2 = new Student(2,"Kiro","Kirov");
		Student s3 = new Student(3,"Mile","Milevski");
		
		service.addStudent(s1);
		service.addStudent(s2);
		service.addStudent(s3);
		
		List<Student> list = service.findAll();
		System.out.println(list);
		
		Student s = service.getStudent(2);
		System.out.println(s);
		Student ss = service.removeStudent(2);
		System.out.println(ss +" Deleted");
		System.out.println(list);
		

	}

}
